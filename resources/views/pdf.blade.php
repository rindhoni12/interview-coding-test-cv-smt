<!DOCTYPE html>
<html>

<head>
    <title>{{$title}}</title>
</head>

<body>
    <style>
        table,
        td,
        th {
            border: 1px solid #ddd;
            text-align: left;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th,
        td {
            padding: 15px;
            text-align: center;
        }
    </style>
    <center>
        <h2>{{$title}}</h2>
        <p>Dieksport pada : {{$date}}</p>
    </center>

    <table class='table table-bordered'>
        <thead>
            <tr>
                <th>No</th>
                <th>NIM</th>
                <th>Nama</th>
                <th>Jurusan</th>
                <th>Tempat Lahir</th>
                <th>Tanggal Lahir</th>
                <th>Alamat</th>
            </tr>
        </thead>
        <tbody>
            @php $i=1 @endphp
            @foreach($mahasiswa as $data)
            <tr>
                <td>{{ $i++ }}</td>
                <td>{{$data->nim}}</td>
                <td>{{$data->nama}}</td>
                <td>{{$data->nama_jurusan}}</td>
                <td>{{$data->tempat_lahir}}</td>
                <td>{{$data->tgl_lahir}}</td>
                <td>{{$data->alamat}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>

</body>

</html>