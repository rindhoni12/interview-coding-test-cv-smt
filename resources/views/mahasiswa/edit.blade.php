@extends('components.master')

@section('main')
<main>
    <div class="container-fluid px-4">
        <h1 class="mt-4">Tables</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{ route('index') }}">Dashboard</a></li>
            <li class="breadcrumb-item active">Mahasiswa</li>
        </ol>
        <div class="card mb-4">
            <div class="card-body">
                <form action="{{ route('update', $mahasiswa) }}" method="POST">
                    @csrf
                    @method('patch')
                    <div class="form-group mb-2">
                        <label>NIM</label>
                        <input type="text" name="nim" class="form-control"
                            value="{{ old('name') ?? $mahasiswa->nim }}" />
                    </div>
                    <div class="form-group mb-2">
                        <label>Nama</label>
                        <input type="text" name="nama" class="form-control"
                            value="{{ old('name') ?? $mahasiswa->nama }}" />
                    </div>
                    <div class="form-group mb-2">
                        <label>Jurusan</label>
                        <select name="nama_jurusan" id="nama_jurusan" class="form-control">
                            <option disabled selected>Pilih jurusan</option>
                            @foreach ($jurusans as $item)
                            <option {{ $item->nama_jurusan == $mahasiswa->nama_jurusan ? 'selected' : '' }}
                                value="{{$item->nama_jurusan}}">
                                {{ $item->nama_jurusan }}
                            </option>
                            @endforeach
                        </select>
                        {{--
                        <input type="text" name="nama_jurusan" class="form-control"
                            value="{{ old('name') ?? $mahasiswa->nama_jurusan }}" /> --}}
                    </div>

                    {{-- <div class="form-group mb-2">
                        <label>Tempat Lahir</label>
                        <input type="text" name="tempat_lahir" class="form-control"
                            value="{{ old('name') ?? $mahasiswa->tempat_lahir }}" />
                    </div> --}}
                    <div class="form-group mb-2">
                        <label>Tempat Lahir</label>
                        <select name="tempat_lahir" id="tempat_lahir_select" class="form-control">
                            <option disabled selected>{{ $mahasiswa->tempat_lahir }}</option>
                        </select>
                    </div>


                    <div class="form-group mb-2">
                        <label>Tanggal Lahir</label>
                        <input type="date" name="tgl_lahir" class="form-control"
                            value="{{ old('name') ?? $mahasiswa->tgl_lahir }}" />
                    </div>
                    <div class="form-group mb-2">
                        <label>Alamat</label>
                        <input type="text" name="alamat" class="form-control"
                            value="{{ old('name') ?? $mahasiswa->alamat }}" />
                    </div>
                    <button type="submit" class="btn btn-success mt-2">Simpan</button>
                </form>
            </div>
        </div>

    </div>
</main>
@endsection

@section('add-script')
<script>
    $(document).ready(function() {
        $(document).on('click','#tempat_lahir_select', function() {
            $.ajax({
                url: 'http://api.sevenmediatech.com/kota',
                method: 'GET',
                
                success: function (response) {
                    $.each(response.content, function(i, item) {
                        $('#tempat_lahir_select').append(new Option(response.content[i].name, response.content[i].name))
                    });
                }
            });
        })
    });
</script>
@endsection