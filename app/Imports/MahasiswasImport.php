<?php

namespace App\Imports;

use App\Models\Mahasiswa;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class MahasiswasImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Mahasiswa([
            'nim'           => $row[0],
            'nama'          => $row[1],
            'nama_jurusan'  => $row[2],
            'tempat_lahir'  => $row[3],
            'tgl_lahir'     => $row[4],
            'alamat'        => $row[5]
        ]);
    }
}
