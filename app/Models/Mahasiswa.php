<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    // use HasFactory;
    protected $fillable = ['nim', 'nama', 'nama_jurusan', 'tempat_lahir', 'tgl_lahir', 'alamat'];

    protected $table = 'mahasiswas';
    public $timestamps = true;
}
