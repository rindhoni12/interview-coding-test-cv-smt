<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{ Mahasiswa, Jurusan };
use PDF;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\MahasiswasImport;
use App\Exports\MahasiswasExport;

class MahasiswaController extends Controller
{
    public function index()
    {
        $mahasiswa_ti_above_21 = Mahasiswa::where('tgl_lahir', '<=', date('Y-m-d', strtotime('-10 years')))
                                            ->where('nama_jurusan', 'Teknik Informatika')
                                            ->get();        
        // dd($mahasiswa_ti_above_21);

        return view('mahasiswa', compact('mahasiswa_ti_above_21'));
    }

    public function create()
    {
        $jurusans = Jurusan::all();
        return view('mahasiswa.create', [
            'jurusans'   => Jurusan::all()
        ]);
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $mahasiswa = request()->validate([
            'nim'  => 'required',
            'nama' => 'required',
            'nama_jurusan' => 'required',
            'tempat_lahir' => 'required',
            'tgl_lahir' => 'required',
            'alamat' => 'required',
        ]);

        Mahasiswa::create($mahasiswa);
        return redirect('/')->with('success', 'Data berhasil disimpan!');
    }

    public function edit(Mahasiswa $mahasiswa)
    {
        $jurusans = Jurusan::all();
        return view('mahasiswa.edit', [
            'mahasiswa' => $mahasiswa,
            'jurusans'   => Jurusan::all()
        ]);
    }

    public function update(Mahasiswa $mahasiswa)
    {
        $mahasiswas = request()->validate([
            'nim'  => 'required',
            'nama' => 'required',
            'nama_jurusan' => 'required',
            'tempat_lahir' => 'required',
            'tgl_lahir' => 'required',
            'alamat' => 'required',
        ]);

        $mahasiswa->update($mahasiswas);
        return redirect('/')->with('success', 'Data berhasil diubah!');
    }

    public function destroy(Mahasiswa $mahasiswa)
    {
        $mahasiswa->delete();
        return redirect('/')->with('success', 'Data berhasil dihapus!');
    }
    
    public function generatePDF()
    {
        // $data['mahasiswa'] = Mahasiswa::all();        
        $data = [
            'mahasiswa' => Mahasiswa::all(),
            'title' => 'Data Semua Mahasiswa',
            'date' => now()
        ];
        
        $pdf = PDF::loadView('pdf', $data);
    
        return $pdf->download('data-semua-mahasiswa-'.now().'.pdf');
    }

    public function fileImport(Request $request) 
    {
        Excel::import(new MahasiswasImport, $request->file('file')->store('temp'));
        return back();
    }

    public function fileExport() 
    {
        return Excel::download(new MahasiswasExport, 'data-semua-mahasiswa-'.now().'.xlsx');
    }  
}
