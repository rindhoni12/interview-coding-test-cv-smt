<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MahasiswaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [MahasiswaController::class, 'index'])->name('index');
Route::get('/create', [MahasiswaController::class, 'create'])->name('create');
Route::post('/store', [MahasiswaController::class, 'store'])->name('store');

Route::get('/edit/{mahasiswa}', [MahasiswaController::class, 'edit'])->name('edit');
Route::patch('/edit/{mahasiswa}', [MahasiswaController::class, 'update'])->name('update');
Route::delete('/delete/{mahasiswa}', [MahasiswaController::class, 'destroy'])->name('destroy');

// Generate PDF
Route::get('generate-pdf', [MahasiswaController::class, 'generatePDF'])->name('pdf');

// Import and Export Excel
Route::post('file-import', [MahasiswaController::class, 'fileImport'])->name('file-import');
Route::get('file-export', [MahasiswaController::class, 'fileExport'])->name('file-export');
